import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { TextPlugin } from "gsap/TextPlugin";

gsap.registerPlugin(ScrollTrigger, TextPlugin);

gsap.to(".pContent", {
  yPercent: -100,
  ease: "none",
  scrollTrigger: {
    trigger: ".pSection",
    // start: "top bottom", // the default values
    // end: "bottom top",
    scrub: true
  }, 
});

gsap.to(".pImage", {
  yPercent: 50,
  ease: "none",
  scrollTrigger: {
    trigger: ".pSection",
    // start: "top bottom", // the default values
    // end: "bottom top",
    scrub: true
  }, 
});

/* Header / Title h1 & h2 fx - text */
/* gsap.to("h1", {duration: 3, text: "this is a to tween"})
gsap.from("h2", {duration: 3, text: ""}) */

/* SVG gsap effect on text */
let words = gsap.utils.toArray("svg text"),
    tl = gsap.timeline({delay: 0.5}),
    timePerCharacter = 0.2;

words.forEach(el => {
  tl.from(el, {text: "", duration: el.innerHTML.length * timePerCharacter, ease: "none"});
});

/**
 * Méthode pr afficher année actuelle + infos texte
 * Ciblage balise & Récup. contenu existant 
 * & concaténation nouveau contenu & injection
 */
const date = new Date();
const INFOS = document.querySelector("#infos");
INFOS.innerHTML += `
  © ${date.getFullYear()} SARL A.D.M.S.
  <br />
  - Accès Difficile Multi Services -
  <br />
  - code APE : 4399D -
  <br />
  - travaux spécialisés de construction -
  <br />
  ${date.toLocaleDateString()} -
  site bidouillé par hug - 
  <br />
  code en licence open source - 
`;

console.log(`© ${date.getFullYear()} SARL ADMS. Accès Difficile Multi Services.
- TODAY, ${date.toLocaleDateString()} -
jouer à space invader bidouillé par hug`);

            
            