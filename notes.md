# notes
+ Sous la responsabilité du Responsable Informatique vous participerez à toutes les phases des projets de développement web (Extranet, Intranet, Parcours de signature électronique).

Vos missions seront les suivantes :

Concevoir, développer et tester des solutions dans un environnement Agile

Contribuer à la construction d’une infrastructure en microservices

Développer et concevoir des microservices et API RESTful.

Monitorer et être proactif sur l’état de la plateforme

Participer à la veille technologique, ainsi qu’à l’évolution de nos outils et normes de développement.

Être un élément moteur pour l’équipe

+ Conception & développement: 
    - d'API REST & GraphQL
    - de micro services

+ Refonte d'applications avec une achitecture monolitique, pour ... vers une architecture séparée en microservices

+ Paradigme Orienté Objet
+ Paradigme Fonctionnel 
+ Paradigme Fonctionnel Réactif 

+ développer une application, avec une approche orienté objet, puis, avec une approche fonctionnelle...
+ je me suis intéressé à la prog fonctionnelle, tt d'abord, avec le principe des fonctions curry, puis avec la lib. RAMDA.js, & aussi avec les principes de prog fnc qu'on retrouve avec des lib. tel RxJS (la prog réactif est basé sur le paradigme fonctionnel), tel React (principe d'imutabilité, de )

+ passionné par la pratique de la programmation, je m'intéresse, je pratique & j'étudie de plus en plus, différents paradigmes de programmation. 
+ après avoir bcp pratiqué l'OO (dév. fullstack Java), je me suis ensuite intéressé à la pro. réactive & asyncrone, ce qui m'a conduit à étudier la pro. fonctionnel, les principes & concepts. J'adore programmer en OO, puis, en fonctionnelle, & enfin, utiliser l'orienté objet au côté du paradigme fonctionnel, qui permet également un apprentissage moins abrupe de lib. asyncrone (basé sur une pro. fnc) type RxJS. 

+ J'aime particulièrement utiliser Ramda.js, très élégant, efficace.

+ compétences techniques, côté back, je suis dev. JAVA de formation, ayant pratiqué bcp de PHP sur des frameworks MVC (Laravel, Symfony) durant mes études.
- je me suis par la suite spécialisé en FullStack JavaScript, sur des applications MERN (MongoDB, Express, React & NodeJS).
- aujourd'hui, sur la partie Back End, je code principalement avec NodeJS et mon attirence pour la pratique du TypeScript, pour le multi paradigme (RxJS ...) m'a poussé à utiliser NestJS, un framework MVC, très opinâtre, , multi paradigme (avec des principes de l'orienté objet & attaché aux principes SOLID , du fonctionnel & du fonctionnel réactif)

+ EXTRAIT DOC. NEST: 
    - Nest combine des éléments de POO (programmation orientée objet), FP (programmation fonctionnelle) et FRP (programmation réactive fonctionnelle).

NestJS propose soit Express soit Fastify, il embarque RxJS (lib. fonctionnel réactive), 

+ Enfin, un framework, le JS coté serveur, back, possède une solution aux problèmes d'architectures, NodeJS

+ NestJS apporte une architecture d'application, MVC, qui permet de créer des applications testables, évolutives, faiblement couplées et facilement maintenables. 

+ NestJS, fortement inspirée d'Angular, utilisant le TypeScript avec principalement les décorateurs ... 
+ Une archi MVC, Angular, on retrouve dans NestJS la même architecture que celle d'Angular ...

+ Nest ns permet d'utiliser `async` pour les fonctions asyncrones, qui doivent bien sûr renvoyer un fichier `Promise`.
- L'utilisation des puissants gestionnaires de routage Nest permet de renvoyer des flux observables RxJS. Nest s'abonne automatiquement à la source & prend la dernière valeur émise (une fois le flux fini).

+ Nest, qui organise les dépendances de manière plus OO, préconise de suivre fortement les principes SOLID .

On retrouve fortement le principe de Single-responsibility, 

+ Quelques mots sur les lib. & la prog. fonctionnelle avec `Ramda.js`, `Radash` (lib. qu'on pt situer entre `Ramda` & `Lodash`, + accésible que `Ramda`, + tournée fonctionnelle que `Lodash` ...) `Lodash` ou encore `Underscore` ...
    - passionné par la programmation, j'adore coder en JavaScript car c'est un langages multi paradigme, qui n'enferme pas dans un style de programmation. On pt utiliser JavaScript dans un style orienté objet et/ou dans un style fonctionnel. Personnellement, je pratique l'OO & le FNC, étant fan de ce dernier, je le pratique, l'étudie .... En écrivant un code fonctionnel, mon code est d'avantage testable, plus générique, réutilisable, simple & lisible. En écrivant des fonctions qui acceptent des paramètres et produisent une sortie : il n'y a pas d'effets secondaires ! Ainsi, lorsque vous souhaitez écrire un JavaScript fonctionnel, vous devez d'abord faire un choix entre la bibliothèque qui peut vous aider à le faire. 
    
    + Ramda a introduit le principe des fonction CURRY, (en programmation fonctionnel, le CURRY permet de créer une fonction à partir d'une autre fonction en ne fournissant pas tous les arguments).

    - le fnc sont auto. misent au curry avec Ramda, vraiment utile lorsque vous avez besoin de manipuler une structure comme des promesses ou des observables. 