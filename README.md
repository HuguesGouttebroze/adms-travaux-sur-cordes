# ADMS
## Formulaire de contact
+ créer 1 compte sur [emailjs](https://www.emailjs.com/pricing/)
---
## TO DO
- passer tests SEO - tester qualité du réferencement natutrel
- créer compte pr mail/contact

## ASK
+ lien | icones vers réseaux sociaux?
    - face book?
    - linkedin, twiter ... masto...
+ tu veux k les gens fassent des devis en ligne?
+ tu veux 1 carte avec ton adresse en géolocalisation?
+ quelle est la zone d'intervention?

+ ??? pr le favicon, on met koi???

## SEO 
+ pour booster le référencement sur google, faut mettre des liens vers des sites externes, & si possible l'inverse
    - pr linstant ya qu'1 lien, vers le code source (& il est en public)

### Outils / Tools pr SEO
+ `google tag manager`
+ `google analytics`
+ `google analytics AMP`

+ `Next-sitemap` : Sitemap generator for `next.js`. Generate sitemap(s) and robots.txt for all `static/pre-rendered/dynamic/server-side` pages.

+ `NextJS SEO` PLUGIN
    - `NextSeo` fonctionne en l'incluant sur les pages où vous souhaitez ajouter des attributs SEO. Une fois inclus sur la page, vous lui transmettez un objet de configuration avec les propriétés SEO de la page. Cela peut être généré dynamiquement au niveau de la page ou, dans certains cas, votre API peut renvoyer un objet SEO.

    - Installation
    `npm install next-seo`
        ou
    `yarn add next-seo`

    - Ajouter le référencement à la page

        Si vous utilisez le répertoire d'application Next.js, il est fortement recommandé d'utiliser la méthode intégrée generateMetaData. Vous pouvez consulter les docs ici

        Si vous utilisez pagesun annuaire, `NextSeo` c'est exactement ce dont vous avez besoin pour vos besoins de référencement !

        Ensuite, vous devez importer NextSeoet ajouter les propriétés souhaitées. Cela rendra les balises dans le <head> pour le référencement. Au minimum, vous devez ajouter un titre et une description.

    Exemple avec juste un titre et une description :

    ```js
        import { NextSeo } from 'next-seo';

        const Page = () => (
        <>
            <NextSeo
            title="Simple Usage Example"
            description="A short description goes here."
            />
            <p>Simple Usage</p>
        </>
        );

        export default Page;
    ```